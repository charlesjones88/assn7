//Charles Jones
//CS3060-601
//Assignment 7
/*Promise of Originality
* I promise that this source code file has, in it's entirety,
* been written by myself and by no other person or persons. If at any time an
* exact copy of this source code is found to be used by another perosn in
* this term, I understand that both myself and the student that submitted
* the copy will receive a zero on this assignment.*/

#include<stdio.h>
#include<stdlib.h>
#include<math.h>

#define SIZE (100)

void firstCome(int[], int);
void shortestSeek(int[], int);
void look(int[], int);
void circular(int[], int);
int firstNotVisited(int[], int);
int smallest(int[], int);
int largest(int[], int);
void printSeek(int);

int main()
{
    int list[SIZE];
    int size = 0;
    printf("Assignment 7: Block Access Algorithm\nBy: Charles Jones\n\n");
    while(1 == fscanf(stdin, "%d", &list[size] ))
    {
        size++;
    }
    firstCome(list, size);
    shortestSeek(list, size);
	look(list, size);
    circular(list, size);
    return 0; 
}

void firstCome(int list[], int size)
{
    int i, seek = 0, drive = list[0];   
    for(i = 1; i < size; i++)
    {
        seek += abs(drive - list[i]);
        drive = list[i];
    }
    printf("FCFS");
    printSeek(seek);
}

void shortestSeek(int list[], int size)
{
    int closest, i, j, seek = 0, drive = list[0];
    int *visited = malloc(sizeof(int) * size);
    for(i = 0; i < size; i++)   
        visited[i] = 0;
    visited[0] = 1;
    for(i = 0; i < size - 1; i++)
    {
        closest = firstNotVisited(visited, size);
        for(j = closest + 1; j < size; j++)
        {
            if(0 == visited[j])
            {
                if(abs(drive - list[closest]) > abs(drive - list[j]))
                {
                    closest = j;
                }
            }
        }
        visited[closest] = 1;
        seek += abs(drive - list[closest]);
        drive = list[closest];
    }
    printf("SSTF");
    printSeek(seek);
    free(visited);
}

void look(int list[], int size)
{
    int closest = 0, i, j, seek = 0, drive = list[0], smallNum, largeNum;
    int *visited = malloc(sizeof(int) * size);
    for(i = 0; i < size; i++)
        visited[i] = 0;
    visited[0] = 1;
    smallNum = smallest(list, size);
    largeNum = largest(list, size);
    for (i = 0; i < size - 1; i++)
    {
        closest = firstNotVisited(visited, size);
        for(j = closest + 1; j < size; j++)
        {
            if(0 == visited[closest])
            {
                if(abs(drive - smallNum) > abs(drive - largeNum))
                {
                    if(0 == visited[largeNum] && drive < list[j] && (list[j] < list[closest] || (list[j] > list[closest] && list[closest] < drive)))
                        closest = j;
                    else if(0 != visited[largeNum] && 0 == visited[j] && drive > list[j] && (list[j] > list[closest] || (list[j] < list[closest] && list[closest] > drive)))
                        closest = j;
                }
                else
                {
                    if(0 == visited[smallNum] && drive > list[j] && (list[j] > list[closest] || (list[j] < list[closest] && list[closest] > drive)))
                        closest = j;
                    else if(0 != visited[smallNum] && 0 == visited[j] && drive < list[j] && (list[j] < list[closest] || (list[j] > list[closest] && list[closest] < drive)))
                        closest  = j;
            
                }
            }
        }
        visited[closest] = 1;
        seek += abs(drive - list[closest]);
        drive = list[closest];
    }
    printf("LOOK");
    printSeek(seek);
    free(visited);
}

void circular(int list[], int size)
{
    int closest = 0, i, j, seek = 0, drive = list[0], smallNum, largeNum;
    int *visited = malloc(sizeof(int) * size);
    for(i = 0; i < size; i++)
        visited[i] = 0;
    visited[0] = 1;
    smallNum = smallest(list, size);
    largeNum = largest(list, size);
    for(i = 0; i < size; i++)
    {
        closest = firstNotVisited(visited, size);
        for(j = 0; j < size; j++)
        {
            if(0 == visited[closest])
            {
                if(0 == visited[largeNum] && drive < list[j] && (list[j] < list[closest] || (list[j] > list[closest] && list[closest] < drive)))
                    closest = j;
                else if(0 != visited[largeNum] && 0 == visited[smallNum] && 0 == visited[closest] && drive > list[j] && (list[j] < list[closest] && list[closest] > drive))
                    closest = j;
                else if(0 != visited[largeNum] && 0 != visited[smallNum] && 0 == visited[closest] && drive < list[j] && (list[j] < list[closest]))
                    closest = j;
            }
        }
        visited[closest] = 1;
        seek += abs(drive - list[closest]);
        drive = list[closest];
    }
    printf("C-LOOK");
    printSeek(seek);
    free(visited);
}

int firstNotVisited(int visited[], int size)
{
    int i;
    for(i = 0; i < size; i++)
    {
        if(0 == visited[i]) 
        {
            return i;
        }
    }
    return size;
}

int smallest(int list[], int size)
{
    int i, smallestIndex = 0;
    for(i = 0; i < size; i++)
    {
        if(list[i] < list[smallestIndex])
            smallestIndex = i;
    }
    return smallestIndex;
}

int largest(int list[], int size)
{
    int i, largestIndex = 0;
    for(i = 0; i < size; i++)
    {
        if(list[i] > list[largestIndex])
        {
            largestIndex = i;
        }
    }
    return largestIndex;
}

void printSeek(int seek)
{
    printf(" Total Seek: %d\n", seek);
}
